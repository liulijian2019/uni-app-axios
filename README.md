## uni-app-axios
> 基于 Promise 的 HTTP 协议的请求库，适用 uni-app、浏览器和 node 环境

1、 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截

2、支持全局挂载

3、支持多个全局配置实例

4、支持自定义验证器

5、支持文件上传/下载

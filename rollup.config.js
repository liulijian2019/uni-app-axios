import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'

export default {
  input: 'src/axios.js',
  output: {
    // 当format为iife和umd时必须提供，将作为全局变量挂在window(浏览器环境)下：window.A=...
    name: 'axios',
    // 输出路径
    file: 'dist/axios.js',
    // 输出格式
    format: 'umd',
    // 生成bundle.map.js文件，方便调试
    sourcemap:true
  },
  plugins: [
    commonjs(),
    json()
  ]
}
